/*----------  Subsection comment block  ----------*/

/*---------------------------------------*/
/* ⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠ */
/*---------------------------------------*/

/**
 *
 * Block comment
 *
 */


(function() {

  const fruits = [];
  fruits.push("banana", "apple", "peach");
  console.log(fruits.length); // 3

  // array's length property accordingly:

  fruits[5] = "mango";
  console.log(fruits[5]); // 'mango'
  console.log(Object.keys(fruits)); // ['0', '1', '2', '5']
  console.log(fruits.length); // 6

  // Increasing the length.

  fruits.length = 10;
  console.log(fruits); // ['banana', 'apple', 'peach', empty x 2, 'mango', empty x 4]
  console.log(Object.keys(fruits)); // ['0', '1', '2', '5']
  console.log(fruits.length); // 10
  console.log(fruits[8]); // undefined

  // Decreasing the length property does, however, delete elements.

  fruits.length = 2;
  console.log(Object.keys(fruits)); // ['0', '1']
  console.log(fruits.length); // 2

})();



// Generic array methods




/**
 * Generic array methods
 * Array.prototype.join.call(object, ''); //'a+b'
 */

// 🧿 Type Casting 

const arrayLike = {
  0: "a",
  1: "b",
  length: 2,
};

console.log(Array.prototype.join.call(arrayLike, "+")); // 'a+b'

/**
 * Normalization of the length property
 * Array.prototype.flat.call({}); // []
 */

// 🧿 Type Casting 

Array.prototype.flat.call({}); // []

console.log(
  Array.prototype.flat.call({
    0: "a",
    1: "b",
    length: 2,
  })
)
// ['a', 'b']


/**
 * Normalization of the length property
 * Array.prototype.push.call({}); 
 * // It only shows integers('length')
 */

// 🎾 Data Calibration

const a = { weight: 100.7, length: 45.4, name: 'Tom' };
Array.prototype.push.call(a);

console.log(a.weight); // 100.7
console.log(a.length); // 45



/**
 * Array-like objects
 * Array.prototype.join.call(object, '');
 */

// 🧿 Type Casting 

function f() {
  console.log(Array.prototype.join.call(arguments, "+"));
}

f("a", "b"); // 'a+b'

f(['a'], ["b"]); // 'a+b'

f(1, ["b"]); // '1+b'

f('a', ["b"]); // 'a+b'

f(1, 2); // '1+2'

f({ 1: '1' }, { 2: '2' }); // [object Object]+[object Object]


/*----------  Create an array  ----------*/

(function() {

  // 'fruits' array created using array literal notation.
  const fruits = ["Apple", "Banana"];
  console.log(fruits.length);
  // 2

  // 'fruits2' array created using the Array() constructor.
  const fruits2 = new Array("Apple", "Banana");
  console.log(fruits2.length);
  // 2

  // 🧿 Type Casting 

  // 'fruits3' array created using String.prototype.split().
  const fruits3 = "Apple, Banana".split(", ");
  console.log(fruits3.length);
  // 2

})();


/*----------  Create an array  ----------*/

(function() {

  // 🧿 Type Casting 

  const fruits = ["Apple", "Banana"];
  const fruitsString = fruits.join(", ");
  console.log(fruitsString);
  // "Apple, Banana"

})();


/*----------  Access an array item by its index  ----------*/

(function() {

  const fruits = ["Apple", "Banana"];

  // The index of an array's first element is always 0.
  fruits[0]; // Apple

  // The index of an array's second element is always 1.
  fruits[1]; // Banana

  // The index of an array's last element is always one
  // less than the length of the array.
  fruits[fruits.length - 1]; // Banana

  // Using an index number larger than the array's length
  // returns 'undefined'.
  fruits[99]; // undefined

})();



/*----------  Find the index of an item in an array  ----------*/

(function() {

  const fruits = ["Apple", "Banana"];
  console.log(fruits.indexOf("Banana"));
  // 1

})();



/*----------  Check if an array contains a certain item  ----------*/

(function() {

  const fruits = ["Apple", "Banana"];

  fruits.includes("Banana"); // true
  fruits.includes("Cherry"); // false

  // If indexOf() doesn't return -1, the array contains the given item.
  fruits.indexOf("Banana") !== -1; // true
  fruits.indexOf("Cherry") !== -1; // false

})();


/*----------  Append an item to an array  ----------*/

(function() {

  const fruits = ["Apple", "Banana"];
  const newLength = fruits.push("Orange");
  console.log(fruits);
  // ["Apple", "Banana", "Orange"]
  console.log(newLength);
  // 3

})();



/*----------  Remove the last item from an array  ----------*/

(function() {

  const fruits = ["Apple", "Banana", "Orange"];
  const removedItem = fruits.pop();
  console.log(fruits);
  // ["Apple", "Banana"]
  console.log(removedItem);
  // Orange

})();





/*----------  Remove multiple items from the end of an array  ----------*/

(function() {

  const fruits = ["Apple", "Banana", "Strawberry", "Mango", "Cherry"];
  const start = -3;
  const removedItems = fruits.splice(start);
  console.log(fruits);
  // ["Apple", "Banana"]
  console.log(removedItems);
  // ["Strawberry", "Mango", "Cherry"]

})();



/*----------  Truncate an array down to just its first N items  ----------*/

(function() {

  const fruits = ["Apple", "Banana", "Strawberry", "Mango", "Cherry"];
  const start = 2;
  const removedItems = fruits.splice(start);
  console.log(fruits);
  // ["Apple", "Banana"]
  console.log(removedItems);
  // ["Strawberry", "Mango", "Cherry"]

})();



/*----------  Remove the first item from an array  ----------*/

(function() {

  const fruits = ["Apple", "Banana"];
  const removedItem = fruits.shift();
  console.log(fruits);
  // ["Banana"]
  console.log(removedItem);
  // Apple

})();



/*----------  Remove multiple items from the beginning of an array  ----------*/

(function() {

  const fruits = ["Apple", "Strawberry", "Cherry", "Banana", "Mango"];
  const start = 0;
  const deleteCount = 3;
  const removedItems = fruits.splice(start, deleteCount);
  console.log(fruits);
  // ["Banana", "Mango"]
  console.log(removedItems);
  // ["Apple", "Strawberry", "Cherry"]


})();



/*----------  Add a new first item to an array  ----------*/

(function() {

  const fruits = ["Banana", "Mango"];
  const newLength = fruits.unshift("Strawberry");
  console.log(fruits);
  // ["Strawberry", "Banana", "Mango"]
  console.log(newLength);
  // 3

})();



/*----------  Remove a single item by index  ----------*/

(function() {

  const fruits = ["Strawberry", "Banana", "Mango"];
  const start = fruits.indexOf("Banana");
  const deleteCount = 1;
  const removedItems = fruits.splice(start, deleteCount);
  console.log(fruits);
  // ["Strawberry", "Mango"]
  console.log(removedItems);
  // ["Banana"]

})();




/*----------  Remove multiple items by index  ----------*/

(function() {

  const fruits = ["Apple", "Banana", "Strawberry", "Mango"];
  const start = 1;
  const deleteCount = 2;
  const removedItems = fruits.splice(start, deleteCount);
  console.log(fruits);
  // ["Apple", "Mango"]
  console.log(removedItems);
  // ["Banana", "Strawberry"]

})();




/*----------  Replace multiple items in an array  ----------*/

(function() {

  const fruits = ["Apple", "Banana", "Strawberry"];
  const start = -2;
  const deleteCount = 2;
  const removedItems = fruits.splice(start, deleteCount, "Mango", "Cherry");
  console.log(fruits);
  // ["Apple", "Mango", "Cherry"]
  console.log(removedItems);
  // ["Banana", "Strawberry"]

})();




/*----------  Iterate over an array  ----------*/

(function() {

  const fruits = ["Apple", "Mango", "Cherry"];
  for (const fruit of fruits) {
    console.log(fruit);
  }
  // Apple
  // Mango
  // Cherry

})();



// 🔫 To display additional indexes when using 'A'

/*----------  Call a function on each element in an array  ----------*/

(function() {

  const fruits = ["Apple", "Mango", "Cherry"];
  fruits.forEach((item, index, array) => {
    console.log(item, index);
  });
  // Apple 0
  // Mango 1
  // Cherry 2

})();



console.clear();

/*----------  Merge multiple arrays together  ----------*/

(function() {

  const fruits = ["Apple", "Banana", "Strawberry"];
  const moreFruits = ["Mango", "Cherry"];
  const combinedFruits = fruits.concat(moreFruits);
  console.log(combinedFruits);
  // ["Apple", "Banana", "Strawberry", "Mango", "Cherry"]

  // The 'fruits' array remains unchanged.
  console.log(fruits);
  // ["Apple", "Banana", "Strawberry"]

  // The 'moreFruits' array also remains unchanged.
  console.log(moreFruits);
  // ["Mango", "Cherry"]

})();

console.log("\n\n\n\n\n\n\n\n\n\n");

/*----------  Copy an array  ----------*/

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#copy_an_array

/*---------------*/
/* Copy an array */
/*---------------*/

// 📌 배열복사하기(얕은 복사와 깊은 복사)
// 1. 리터럴 배열 형태로 하나의 배열을 선언한다. `const fruits = ["Strawberry", "Mango"];`
// 2. 신규 변수를 하나 추가하여 이전 배열을 할당한다. `const fruitsAlias = fruits;`
// 3. 기존 배열과 신규 배열을 엄격히 비교(===, 일치연산자)한다. `fruits === fruitsAlias;`
// 4. 기존 배열에 요소를 추가한다. `fruits.unshift("Apple", "Banana");`
// 5. 기존 배열의 요소를 화면에 출력한다. `console.log(fruits);`
// 6. 신규 배열의 요소를 화면에 출력한다. `console.log(fruitsAlias);`

// 모든 요소의 '깊은 복사', 즉 중첩 배열과 객체 요소 또한 동일한 형태로 복사하는 방법 중 하나는 
// JSON.stringify()를 사용해 배열을 JSON 문자열로 변환한 후, 
// JSON.parse()로 다시 배열을 구성하는 것입니다.

(function() {

  console.log(
    `✓ Spread syntax (...), Array.from(), Array.prototype.slice(), JSON.stringify(), JSON.parse() \n%cCopy an array(Swallow Copy & Deep Copy)`,
    "color: yellow; font-style: italic; font-size: 1em; background-color: blue; padding: 2px; margin: 5px 0;"
  );

  console.log(`
    %c배열을 새로운 변수에 직접 할당해도 배열이 복사되지는 않습니다. 
    `, 'color:#D73A49');

  console.log(`
    %c새로운 변수에는 원본 배열을 가리키는 참조만 할당되며, 
    원본 배열의 값을 바꾸면 새 변수에서도 그 변경점이 반영됩니다.
    `, 'color:gray');

  console.log(`
    %c배열의 복사본을 만들기 위해서는 새 배열을 위한 변수를 생성하고, 
    원본 배열 각각의 원시 요소에 대해서도 새로운 변수를 생성해야 합니다. 
    `, 'color:#D73A49');

  console.log(`
    %c(변수를 원시 값으로 초기화하면 참조를 할당하지 않고 값을 복사합니다.) 
    JavaScript에서는 이를 위해 다음과 같은 방법을 사용할 수 있습니다.
    `, 'color:gray');


  console.log(`
    %c모든 요소의 '깊은 복사', 즉 중첩 배열과 객체 요소 또한 동일한 형태로 복사하는 방법 중 하나는 
    JSON.stringify()를 사용해 배열을 JSON 문자열로 변환한 후, 
    JSON.parse()로 다시 배열을 구성하는 것입니다.
    `, 'color:#D73A49');

  const fruits = ["Strawberry", "Mango"];

  // Create a copy using spread syntax.
  const fruitsCopy = [...fruits];
  // ["Strawberry", "Mango"]

  // Create a copy using the from() method.
  const fruitsCopy2 = Array.from(fruits);
  // ["Strawberry", "Mango"]

  // Create a copy using the slice() method.
  const fruitsCopy3 = fruits.slice();
  // ["Strawberry", "Mango"]

  // 위의 세 코드는 모두 '얕은 복사'를 생성합니다. 
  // 얕은 복사란 배열의 최상위 요소가 원시 값일 경우 복사하지만, 
  // 중첩 배열이나 객체 요소일 경우에는 원본 배열의 요소를 참조하게 됩니다.

  // 🪀 DeepCopy
  const fruitsDeepCopy = JSON.parse(JSON.stringify(fruits));

  // 모든 요소의 '깊은 복사', 즉 중첩 배열과 객체 요소 또한 동일한 형태로 복사하는 방법 중 하나는 
  // JSON.stringify()를 사용해 배열을 JSON 문자열로 변환한 후, 
  // JSON.parse()로 다시 배열을 구성하는 것입니다.

})();

console.log("\n\n\n\n\n\n\n\n\n\n");

/*----------  Create a new variable, then assign it directly  ----------*/

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#copy_an_array

/*------------------------------------------------*/
/* Create a new variable, then assign it directly */
/*------------------------------------------------*/

// 📌 배열복사하기(얕은 복사)
// 1. 리터럴 배열 형태로 하나의 배열을 선언한다. `const fruits = ["Strawberry", "Mango"];`
// 2. 신규 변수를 하나 추가하여 이전 배열을 할당한다. `const fruitsAlias = fruits;`
// 3. 기존 배열과 신규 배열을 엄격히 비교(===, 일치연산자)한다. `fruits === fruitsAlias;`
// 4. 기존 배열에 요소를 추가한다. `fruits.unshift("Apple", "Banana");`
// 5. 기존 배열의 요소를 화면에 출력한다. `console.log(fruits);`
// 6. 신규 배열의 요소를 화면에 출력한다. `console.log(fruitsAlias);`

// 배열을 새로운 변수에 직접 할당해도 배열이 복사되지는 않습니다. 
// 새로운 변수에는 원본 배열을 가리키는 참조만 할당되며, 
// 원본 배열의 값을 바꾸면 새 변수에서도 그 변경점이 반영됩니다.

(function() {

  console.log(
    `✓ Array.prototype.unshift(), Strict equality (===) \n%cCreate a new variable, then assign it directly`,
    "color: yellow; font-style: italic; font-size: 1em; background-color: blue; padding: 2px; margin: 5px 0;"
  );

  console.log(`
    %c배열을 새로운 변수에 직접 할당해도 배열이 복사되지는 않습니다.
    새로운 변수에는 원본 배열을 가리키는 참조만 할당되며, 
    원본 배열의 값을 바꾸면 새 변수에서도 그 변경점이 반영됩니다.
    `, 'color:gray');

  const fruits = ["Strawberry", "Mango"];
  const fruitsAlias = fruits;
  // 'fruits' and 'fruitsAlias' are the same object, strictly equivalent.
  fruits === fruitsAlias; // true
  // Any changes to the 'fruits' array change 'fruitsAlias' too.
  fruits.unshift("Apple", "Banana");
  console.log('%cfruits', 'color:gray', fruits);
  // ['Apple', 'Banana', 'Strawberry', 'Mango']
  console.log('%cfruitsAlias', 'color:gray', fruitsAlias);
  // ['Apple', 'Banana', 'Strawberry', 'Mango']

})();

console.log("\n\n\n\n\n\n\n\n\n\n");

/*---------------------------------------*/
/* ⚠ Grouping the elements of an array ⚠ */
/*---------------------------------------*/

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#creating_a_two-dimensional_array

/*----------------------------------*/
/* Creating a two-dimensional array */
/*----------------------------------*/

// 📌 2차원 배열 만들기
// 1. 리터럴 배열 형태로 완전한 폼의 2차원 배열을 생성한다.
// 2. console.log()로 Template literals(`content`)를 화면에 표시(\n\n: 콘솔에서 줄바꿈 2회)한다.
// 2.1 array.join("\n"): 2차원 배열안에 있는 배열을 결합한다. 
// 2.2 배열 결합시('\n': 콘솔에서 줄바꿈 1회)하여 결합한다. 
// 3. 2차원 배열의 우항의 값을 좌항에 대입(지정)한다. board[4][4] = board[6][4];
// 4. 위의 변경된 내용을 적용하여 화면에 표시한다.

// Move King's Pawn forward 2
// 1. board[4][4] = board[6][4];
// 1.1 board[4][4] = " ";
// 1.2 board[6][4] = "p";
// 1.3 board[4][4] = "p";
// 2. board[6][4] = " ";
// 3. console.log()로 Template literals(`content`)를 화면에 표시한다.
// 3.1 array.join("\n"): 2차원 배열안에 있는 배열을 결합한다.
// 3.2 배열 결합시('\n': 콘솔에서 줄바꿈 1회)하여 결합한다. 

(function() {

  console.log(
    `✓ Array literals, Template literals, Array.prototype.join(), \nSpecial characters in regular expressions. \n%cCreating a two-dimensional array`,
    "color: yellow; font-style: italic; font-size: 1em; background-color: blue; padding: 2px; margin: 5px 0;"
  );

  const board = [
    ["R", "N", "B", "Q", "K", "B", "N", "R"],
    ["P", "P", "P", "P", "P", "P", "P", "P"],
    [" ", " ", " ", " ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " ", " ", " ", " "],
    ["p", "p", "p", "p", "p", "p", "p", "p"],
    ["r", "n", "b", "q", "k", "b", "n", "r"],
  ];

  console.log(`${board.join("\n")}\n\n`);

  // Move King's Pawn forward 2
  board[4][4] = board[6][4];
  board[6][4] = " ";
  console.log(board.join("\n"));

})();

console.log("\n\n\n\n\n\n\n\n\n\n");

// https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Array#%EC%A0%95%EA%B7%9C%ED%91%9C%ED%98%84%EC%8B%9D_%EC%9D%BC%EC%B9%98_%EA%B2%B0%EA%B3%BC%EB%A5%BC_%EC%9D%B4%EC%9A%A9%ED%95%9C_%EB%B0%B0%EC%97%B4_%EC%83%9D%EC%84%B1

/*--------------------------------------------*/
/* Using an array to tabulate a set of values */
/*--------------------------------------------*/

// 📌 배열을 사용하여 일련의 값을 테이블 처럼 표시하기
// 1. 빈 배열을 생성한다. `const values = [];`
// 2. 반복문을 선언한다. `for (let x = 0; x < 10; x++) { ... }`
// 3. 반복문을 통하여 정의한 코드의 결과 값을 빈 배열에 추가한다. `values.push([2 ** x, 2 * x ** 2]);`
// 4. 배열의 데이터를 표로 출력한다. `console.table(values);`

(function() {

  console.log(
    `✓ Declare array, for, Array.prototype.push(), console.table() \n%cUsing an array to tabulate a set of values`,
    "color: yellow; font-style: italic; font-size: 1em; background-color: blue; padding: 2px; margin: 5px 0;"
  );

  const values = [];
  for (let x = 0; x < 10; x++) {
    values.push([2 ** x, 2 * x ** 2]);
  }

  console.log('%c배열 데이터를 표의 형태로 출력한다.', 'color:gray');
  console.table(values);

  // 연산자 우선 순위
  // 2 ** 0 = 1, 0 ** 2 = 1

  // 2 ** 0, 2 * (0 ** 2)
  // 2 ** 1, 2 * (1 ** 2)
  // 2 ** 2, 2 * (2 ** 2)
  // 2 ** 3, 2 * (3 ** 2)
  // 2 ** 4, 2 * (4 ** 2)
  // 2 ** 5, 2 * (5 ** 2)
  // 2 ** 6, 2 * (6 ** 2)
  // 2 ** 7, 2 * (7 ** 2)
  // 2 ** 8, 2 * (8 ** 2)
  // 2 ** 9, 2 * (9 ** 2)

  // 0 1      0
  // 1 2      4
  // 2 4      8
  // 3 8      18
  // 4 16     32
  // 5 32     50
  // 6 64     72
  // 7 128    98
  // 8 256    128
  // 9 512    162

})();

console.log("\n\n\n\n\n\n\n\n\n\n");

// https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Array#%EC%A0%95%EA%B7%9C%ED%91%9C%ED%98%84%EC%8B%9D_%EC%9D%BC%EC%B9%98_%EA%B2%B0%EA%B3%BC%EB%A5%BC_%EC%9D%B4%EC%9A%A9%ED%95%9C_%EB%B0%B0%EC%97%B4_%EC%83%9D%EC%84%B1

/*-----------------------------------------------*/
/* Creating an array using the result of a match */
/*-----------------------------------------------*/

// 🧿 Type Casting(String → (RegExp) → Array)

// 📌 정규표현식 일치 결과를 이용한 배열 생성
// 1. 문자열 ('cdbBdbsbz')
// 2. 정규표현식 (/d(b+)(d)/i)
// 3. 결과 ([ "dbBd", "bB", "d" ])

(function() {

  console.log(
    `✓ Regular expression \n%cCreating an array using the result of a match`,
    "color: yellow; font-style: italic; font-size: 1em; background-color: blue; padding: 2px; margin: 5px 0;"
  );

  // Match one d followed by one or more b's followed by one d
  // Remember matched b's and the following d
  // Ignore case

  // 하나의 d와 하나 이상의 b에 하나의 d가 뒤따라 일치해야 함
  // 일치한 b와 다음 d를 기억할 것
  // 대소문자 구분 없음

  const myRe = /d(b+)(d)/i;
  const execResult = myRe.exec("cdbBdbsbz");

  console.log('%c원본 문자열', 'color:gray', execResult.input); // 'cdbBdbsbz'
  console.log('%c원본 문자열에서 일치가 위치한 인덱스', 'color:gray', execResult.index); // 1
  console.log('%c[0] → 마지막으로 일치한 텍스트 \n[1], ... [n]존재할 경우, \n정규표현식에서 괄호로 지정한 부분문자열 일치에 대응하는 요소\n', 'color:gray', execResult); // [ "dbBd", "bB", "d" ]

  // input → 정규표현식 일치 대상이 된 원본 문자열입니다.
  // index → 일치가 위치한 원본 문자열에서의 인덱스입니다.
  // [0] → 마지막으로 일치한 텍스트입니다.
  // [1], ... [n] → 존재할 경우, 정규표현식에서 괄호로 지정한 부분문자열 일치에 대응하는 요소입니다. 가능한 수의 제한은 없습니다.

})();